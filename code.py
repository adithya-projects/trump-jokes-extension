# Importing the necessary modules
import requests
import vscode
import time

# Main
ext = vscode.Extension(name = "jokes", display_name = "jokes", version = "0.0.2")


@ext.event
def on_activate():
    return "Trump Jokes activated"

@ext.command()    
def Trump_Joke():
    options = vscode.window.show_info_message("Select:" , "Trump Joke with your Name")
    if options == "Trump Joke with your Name":
        name = vscode.window.show_input_box(vscode.ext.InputBoxOptions("Enter your name: "))
        link = "https://api.whatdoestrumpthink.com/api/v1/quotes/personalized?q=" + name 
        response = requests.get(link)
        var = response.json()
        var2 = var['message']
        vscode.window.show_info_message(var2)
        
                             
    
vscode.build(ext)    